package com.java2e.resource.controller;

import io.jsonwebtoken.Jwts;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;

/**
 * @author will.tuo
 * @date 2022/2/15 17:31
 */
@RestController
public class Controller {
    // 添加一个测试访问接口
    @GetMapping("/user")
//    @PreAuthorize("hasRole('test')")
    public Authentication getUser(Authentication authentication) {
        System.out.println("resource: user {}"+ authentication);
        return authentication;
    }
    @GetMapping("/user1")
    @PreAuthorize("hasRole('test')")
    public String getUser1(Authentication authentication) {
        System.out.println("resource: user1 {}"+ authentication);
        return "nfdjfn";
    }

    @GetMapping("/user2")
    @PreAuthorize("hasRole('test1')")
    public String getUser2(Authentication authentication) {
        System.out.println("resource: user2 {}"+ authentication);
        return "nfdjfngcsdgesg";
    }

    @GetMapping("/token")
    public Object getToken(Authentication authentication, HttpServletRequest request){
        String head = request.getHeader("Authorization");
        String token = head.substring(head.indexOf("bearer")+7);
        return Jwts.parser()
                .setSigningKey("jwt".getBytes(StandardCharsets.UTF_8))
                .parseClaimsJws(token)
                .getBody();
    }
}
