package com.java2e.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: liangcan
 * @version: 1.0
 * @date: 2019/5/20 17:45
 * @describtion: ResourceApplication
 */
@SpringBootApplication
@EnableResourceServer
/**
 * 要想在方法上面的@PreAuthority注解生效就需要开启这个注解
 */
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class ResourceApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(ResourceApplication.class)
                .run(args);
    }


}
