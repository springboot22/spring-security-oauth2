package com.java2e.sso.controller;

import io.jsonwebtoken.Jwts;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;

/**
 * @author will.tuo
 * @date 2022/2/15 17:31
 */
@RestController
public class Controller {
    // 添加一个测试访问接口
    @GetMapping("/user")
    public Authentication getUser(Authentication authentication) {
        System.out.println("resource: user {}"+ authentication);
        return authentication;
    }

    @GetMapping("/token")
    public Object getToken(Authentication authentication, HttpServletRequest request){
        String head = request.getHeader("Authorization");
        String token = head.substring(head.indexOf("bearer")+7);
        return Jwts.parser()
                .setSigningKey("jwt".getBytes(StandardCharsets.UTF_8))
                .parseClaimsJws(token)
                .getBody();
    }
}
